Vagrant.configure('2') do |config|
  config.vm.box = 'azure'

  # use local ssh key to connect to remote vagrant box
  config.ssh.username = 'admin001'
  config.ssh.private_key_path = '~/.ssh/rmontag_airplus.id_rsa'
  config.vm.provider :azure do |azure, override|

    # each of the below values will default to use the env vars named as below if not specified explicitly
    azure.tenant_id = 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx'
    azure.client_id = 'yyyyyyyy-yyyy-yyyy-yyyy-yyyyyyyyyyyy'
    azure.client_secret = 'zzzzzzzz-zzzz-zzzz-zzzz-zzzzzzzzzzzz'
    azure.endpoint = 'https://management.core.cloudapi.de/'
    azure.subscription_id  = 'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa'
    azure.location = 'germanycentral'
    azure.resource_group_name = 'ex532ResourceGroup'
    azure.environment = 'german'
  end

  config.vm.provision 'shell', inline: 'echo OHAI'
end
